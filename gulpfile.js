let project_folder = require("path").basename(__dirname);
let source_folder = "#src";

let path = {
   build: {
      html: project_folder + "/",
      css: project_folder + "/css/",
      js: project_folder + "/js/",
      slick: project_folder + "/js/",
      img: project_folder + "/assets/img/",
      fonts: project_folder + "/assets/fonts/",
   },
   src: {
      html: [source_folder + "/*.html", "!" + source_folder + "/_*.html"],
      css: source_folder + "/scss/style.scss",
      js: source_folder + "/js/script.js",
      slick: source_folder + "/js/slick.min.js",
      img: source_folder + "/assets/img/**/*.{jpg,png,svg,gif,ico,webp}",
      fonts: source_folder + "/assets/fonts/*.ttf",
   },
   watch: {
      html: source_folder + "/**/*.html",
      css: source_folder + "/scss/**/*.scss",
      js: source_folder + "/js/**/*.js",
      img: source_folder + "/assets/img/**/*.{jpg,png,svg,gif,ico,webp}",
   },
   clean: "./" + project_folder + "/"
}

//!Объявление переменных и присваивание им значения установленных плагинов
let { src, dest } = require('gulp'),
   gulp = require('gulp'),
   browsersync = require("browser-sync").create(),
   fileinclude = require("gulp-file-include"),
   del = require("del"),
   scss = require("gulp-sass"),
   autoprefixer = require("gulp-autoprefixer"),
   group_media = require("gulp-group-css-media-queries"),
   clean_css = require("gulp-clean-css"),
   rename = require("gulp-rename"),
   uglify = require("gulp-uglify-es").default,
   imagemin = require("gulp-imagemin"),
   webp = require("gulp-webp"),
   webphtml = require("gulp-webp-html"),
   webpcss = require("gulp-webp-css"),
   ttf2woff = require("gulp-ttf2woff"),
   ttf2woff2 = require("gulp-ttf2woff2"),
   fonter = require("gulp-fonter");


//!Постоянное обновление страницы после изменений
function browserSync(params) {
   browsersync.init({
      server: {
         baseDir: "./" + project_folder + "/" //?Путь в итоговую папку проекта
      },
      port: 3000, //?Номер порта для обращения к браузеру
      notify: false //?Чтобы не всплывало уведомление об очередном обновлении страницы
   })
}

//!Работа с файлом .html
function html() {
   return src(path.src.html) //?Путь откуда копировать
      .pipe(fileinclude()) //?Компиляция нескольких .html в один
      .pipe(webphtml())
      .pipe(dest(path.build.html)) //?Вывод итогового файла .html в директорию
      .pipe(browsersync.stream())
}

//!Работа с файлом .css
function css() {
   return src(path.src.css) //?Путь откуда копировать
      .pipe(
         scss({
            outputStyle: "expanded" //!
         })
      )
      .pipe(
         group_media() //?Компановка всех медиа-запросов в группу в конце кода и их оптимизация
      )
      .pipe(
         autoprefixer({ //?Расстановка автопрефиксов для 5 последних версий браузеров
            overrideBrowserslist: ["last 5 versions"],
            cascade: true
         })
      )
      .pipe(webpcss())
      .pipe(dest(path.build.css)) //?Вывод итогового .css файла в директорию
      .pipe(clean_css()) //?Преобразование кода .css в сжатый вид
      .pipe(
         rename({
            extname: ".min.css" //?Установка расширения для нового файла
         })
      )
      .pipe(dest(path.build.css)) //? Вывод итогового сжатого файла в директорию
      .pipe(browsersync.stream())
}

//!Работа с файлом js
function js() {
   return src(path.src.js) //?Путь откуда копировать
      .pipe(fileinclude()) //?Компиляция нескольких файлов .js в один
      .pipe(dest(path.build.js)) //?Вывод итогового файла в директорию для удобного редактирования в будущем
      .pipe(
         uglify() //? Преобразование .js в сжатый вид
      )
      .pipe(
         rename({
            extname: ".min.js" //?Переименование расширения нового файла
         })
      )
      .pipe(dest(path.build.js)) //?Вывод итогового сжатого файла в директорию
      .pipe(browsersync.stream()) //!
}

//!Работа с файлом slick.min.js
function slick() {
   return src(path.src.slick) //?Путь откуда копировать
      .pipe(dest(path.build.slick)) //?Вывод итогового файла в директорию для удобного редактирования в будущем
}

//!Работа с картинками
function images() {
   return src(path.src.img) //?Путь откуда копировать
      .pipe(
         webp({
            quality: 60
         })
      )
      .pipe(dest(path.build.img))
      .pipe(src(path.src.img))
      .pipe(
         imagemin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            interlaced: true,
            optimizationLevel: 3 // от 0 до 7 
         })
      )
      .pipe(dest(path.build.img))
      .pipe(browsersync.stream())
}


//!Слежение за изменениями в реальном времени
function watchFiles(params) {
   gulp.watch([path.watch.html], html); //?за файлом .html
   gulp.watch([path.watch.css], css); //?за файлом .css
   gulp.watch([path.watch.js], js); //?за файлом .js
   gulp.watch([path.watch.img], images); //?за файломи картинок
}

//!Удаление папки dist перед новой компиляцией, дабы убрать ненужные файлы
function clean(params) {
   return del(path.clean);
}

//!Выполнение созданных выше функций
let build = gulp.series(clean, gulp.parallel(js, slick, css, html, images));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.images = images;
exports.js = js;
exports.slick = slick;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch;