
//!=============СЛАЙДЕРЫ=====================
//?=============КЛИЕНТЫ======================
$(document).ready(function () {
   $('.clientsSlider__body').slick({
      speed: 1000,
      autoplay: false,
      autoplaySpeed: 2000,
      touchThreshold: 10,
      arrows: true,
      centerMode: true,
      centerPadding: '20px',
      slidesToShow: 3,
      responsive: [
         {
            breakpoint: 768,
            settings: {
               arrows: false,
            },
            breakpoint: 620,
            settings: {
               slidesToShow: 1,
               arrows: false,
            }
         }]
   });
   $('.clientsSlider__body').slick('setPosition');
});
//?=============ЛИЧНЫЕ КАРТОЧКИ======================
$(document).ready(function () {
   $('.whoWeAre__sliderSkills').slick({
      slidesToShow: 1,
      asNavFor: '.whoWeAre__sliderPersonalCard',
      speed: 1000,
      arrows: false,
      dots: false,
      fade: true,
   });
   $('.whoWeAre__sliderPersonalCard').slick({
      slidesToShow: 1,
      asNavFor: '.whoWeAre__sliderSkills',
      speed: 1000,
      arrows: false,
      dots: true,
   });
});
//!============БУРГЕР ШАПКИ САЙТА===================
$(document).ready(function () {
   $('.header__burger').click(function (event) {
      $('.header__burger,.header__burgerLine_firstLine,.header__burgerLine_centerLine,.header__burgerLine_lastLine').toggleClass('active');
      $('.header__navigationMenu').toggleClass('active');
      $('.header__logo').toggleClass('active');
   });
});
//!============ПЛАВНОСТЬ ЯКОРЕЙ В НАВ МЕНЮ===================
$(document).ready(function () {
   $("#navMenu").on("click", "a", function (event) {
      event.preventDefault();
      var id = $(this).attr('href'),
         top = $(id).offset().top;
      $('body,html').animate({ scrollTop: top }, 500);
   });
});
//!============ВИДЕОРОЛИК===================
const videoBlock = document.querySelector(".aboutUs__blockVideoPlayer");
const buttonPlayNow = document.querySelector(".aboutUs__blockVideoText_playNow");
const video = document.getElementById("video");

buttonPlayNow.addEventListener("click", function () {
   videoBlock.classList.toggle("played");
   video.classList.toggle("played");
   buttonPlayNow.classList.toggle("played");
   if (buttonPlayNow.classList.contains("played")) {
      buttonPlayNow.innerHTML = "STOP NOW";
   } else {
      buttonPlayNow.innerHTML = "PLAY NOW";
   }

});
videoBlock.addEventListener("click", function () {
   videoBlock.classList.toggle("played");
   video.classList.toggle("played");
});
//!============ПУНКТЫ OUR WORKS===================
const allProjectsPoint = document.getElementById("allProjectsPoint");
const photographyPoint = document.getElementById("photographyPoint");
const webDesignPoint = document.getElementById("webDesignPoint");
const brandingPoint = document.getElementById("brandingPoint");
const mobileAppPoint = document.getElementById("mobileAppPoint");
const allProjectsContent = document.getElementById("allProjectsContent");
const photographyContent = document.getElementById("photographyContent");
const webDesignContent = document.getElementById("webDesignContent");
const brandingContent = document.getElementById("brandingContent");
const mobileAppContent = document.getElementById("mobileAppContent");

function addClassActiveOnLoad() {
   allProjectsPoint.classList.add("active");
   allProjectsContent.classList.add("active");
}

window.addEventListener("load", addClassActiveOnLoad());
function addClassActiveForAllProjectsPoint() {
   allProjectsPoint.classList.add("active");
   allProjectsContent.classList.add("active");
   photographyPoint.classList.remove("active");
   photographyContent.classList.remove("active");
   webDesignPoint.classList.remove("active");
   webDesignContent.classList.remove("active");
   brandingPoint.classList.remove("active");
   brandingContent.classList.remove("active");
   mobileAppPoint.classList.remove("active");
   mobileAppContent.classList.remove("active");
}
function addClassActiveForAllPhotography() {
   photographyPoint.classList.add("active");
   photographyContent.classList.add("active");

   allProjectsPoint.classList.remove("active");
   allProjectsContent.classList.remove("active");
   webDesignPoint.classList.remove("active");
   webDesignContent.classList.remove("active");
   brandingPoint.classList.remove("active");
   brandingContent.classList.remove("active");
   mobileAppPoint.classList.remove("active");
   mobileAppContent.classList.remove("active");
}
function addClassActiveForWebDesignPoint() {
   webDesignPoint.classList.add("active");
   webDesignContent.classList.add("active")

   allProjectsPoint.classList.remove("active");
   allProjectsContent.classList.remove("active")
   photographyPoint.classList.remove("active");
   photographyContent.classList.remove("active")
   brandingPoint.classList.remove("active");
   brandingContent.classList.remove("active")
   mobileAppPoint.classList.remove("active");
   mobileAppContent.classList.remove("active");
}
function addClassActiveForBrandingPoint() {
   brandingPoint.classList.add("active");
   brandingContent.classList.add("active");

   allProjectsPoint.classList.remove("active");
   allProjectsContent.classList.remove("active");
   photographyPoint.classList.remove("active");
   photographyContent.classList.remove("active");
   webDesignPoint.classList.remove("active");
   webDesignContent.classList.remove("active");
   mobileAppPoint.classList.remove("active");
   mobileAppContent.classList.remove("active")
}
function addClassActiveForMobileAppPoint() {
   mobileAppPoint.classList.add("active");
   mobileAppContent.classList.add("active");

   allProjectsPoint.classList.remove("active");
   allProjectsContent.classList.remove("active");
   photographyPoint.classList.remove("active");
   photographyContent.classList.remove("active");
   webDesignPoint.classList.remove("active");
   webDesignContent.classList.remove("active");
   brandingPoint.classList.remove("active");
   brandingContent.classList.remove("active");
}
//! ==========АВТОМАТИЧЕСКОЕ ЗАПОЛНЕНИЕ ПРОГРЕСС-БАРОВ В ЗАВИСИМОСТИ ОТ ТЕКСТОВОГО СОДЕРЖАНИЯ ТЕГА=========
const sliderSkillsItemForFirstPerson = document.querySelector(".sliderSkillsItemForFirstPerson");
const firstPersonSkillLevelPhotoshop = sliderSkillsItemForFirstPerson.getElementsByClassName("skillLevel")[0];
const firstPersonSkillLevelIllustartor = sliderSkillsItemForFirstPerson.getElementsByClassName("skillLevel")[1];
const firstPersonSkillLevelSketch = sliderSkillsItemForFirstPerson.getElementsByClassName("skillLevel")[2];
const firstPersonkillLevelAfterEffects = sliderSkillsItemForFirstPerson.getElementsByClassName("skillLevel")[3];

const firstPersonProgressLevelPhotoshop = sliderSkillsItemForFirstPerson.getElementsByClassName("progressLevel")[0];
const firstPersonProgressLevelIllustrator = sliderSkillsItemForFirstPerson.getElementsByClassName("progressLevel")[1];
const firstPersonProgressLevelSketch = sliderSkillsItemForFirstPerson.getElementsByClassName("progressLevel")[2];
const firstPersonProgressLevelAfterEffects = sliderSkillsItemForFirstPerson.getElementsByClassName("progressLevel")[3];

const sliderSkillsItemForSecondPerson = document.querySelector(".sliderSkillsItemForSecondPerson");
const secondPersonSkillLevelPhotoshop = sliderSkillsItemForSecondPerson.getElementsByClassName("skillLevel")[0];
const secondPersonSkillLevelIllustartor = sliderSkillsItemForSecondPerson.getElementsByClassName("skillLevel")[1];
const secondPersonSkillLevelSketch = sliderSkillsItemForSecondPerson.getElementsByClassName("skillLevel")[2];
const secondPersonkillLevelAfterEffects = sliderSkillsItemForSecondPerson.getElementsByClassName("skillLevel")[3];

const secondPersonProgressLevelPhotoshop = sliderSkillsItemForSecondPerson.getElementsByClassName("progressLevel")[0];
const secondPersonProgressLevelIllustrator = sliderSkillsItemForSecondPerson.getElementsByClassName("progressLevel")[1];
const secondPersonProgressLevelSketch = sliderSkillsItemForSecondPerson.getElementsByClassName("progressLevel")[2];
const secondPersonProgressLevelAfterEffects = sliderSkillsItemForSecondPerson.getElementsByClassName("progressLevel")[3];

const sliderSkillsItemForThirdPerson = document.querySelector(".sliderSkillsItemForThirdPerson");
const thirdPersonSkillLevelPhotoshop = sliderSkillsItemForThirdPerson.getElementsByClassName("skillLevel")[0];
const thirdPersonSkillLevelIllustartor = sliderSkillsItemForThirdPerson.getElementsByClassName("skillLevel")[1];
const thirdPersonSkillLevelSketch = sliderSkillsItemForThirdPerson.getElementsByClassName("skillLevel")[2];
const thirdPersonkillLevelAfterEffects = sliderSkillsItemForThirdPerson.getElementsByClassName("skillLevel")[3];

const thirdPersonProgressLevelPhotoshop = sliderSkillsItemForThirdPerson.getElementsByClassName("progressLevel")[0];
const thirdPersonProgressLevelIllustrator = sliderSkillsItemForThirdPerson.getElementsByClassName("progressLevel")[1];
const thirdPersonProgressLevelSketch = sliderSkillsItemForThirdPerson.getElementsByClassName("progressLevel")[2];
const thirdPersonProgressLevelAfterEffects = sliderSkillsItemForThirdPerson.getElementsByClassName("progressLevel")[3];

function firstPersonProgressBar() {
   firstPersonProgressLevelPhotoshop.style.width = firstPersonSkillLevelPhotoshop.textContent;
   firstPersonProgressLevelIllustrator.style.width = firstPersonSkillLevelIllustartor.textContent;
   firstPersonProgressLevelSketch.style.width = firstPersonSkillLevelSketch.textContent;
   firstPersonProgressLevelAfterEffects.style.width = firstPersonkillLevelAfterEffects.textContent;

   secondPersonProgressLevelPhotoshop.style.width = secondPersonSkillLevelPhotoshop.textContent;
   secondPersonProgressLevelIllustrator.style.width = secondPersonSkillLevelIllustartor.textContent;
   secondPersonProgressLevelSketch.style.width = secondPersonSkillLevelSketch.textContent;
   secondPersonProgressLevelAfterEffects.style.width = secondPersonkillLevelAfterEffects.textContent;

   thirdPersonProgressLevelPhotoshop.style.width = thirdPersonSkillLevelPhotoshop.textContent;
   thirdPersonProgressLevelIllustrator.style.width = thirdPersonSkillLevelIllustartor.textContent;
   thirdPersonProgressLevelSketch.style.width = thirdPersonSkillLevelSketch.textContent;
   thirdPersonProgressLevelAfterEffects.style.width = thirdPersonkillLevelAfterEffects.textContent;
};
window.addEventListener("load", firstPersonProgressBar());
$(document).ready(function () {
   $('.locateOnMap__title').click(function (event) {
      $('.locateOnMap__title_arrow,.weOnMap').toggleClass('opened');
   });
});